﻿using System;

namespace StudioKit.Data.Interfaces
{
	public interface IAuditable
	{
		DateTime DateStored { get; set; }

		DateTime DateLastUpdated { get; set; }

		string LastUpdatedById { get; set; }
	}
}