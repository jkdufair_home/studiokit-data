﻿using System;

namespace StudioKit.Data.Attributes
{
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
	public class CaseSensitiveAttribute : Attribute
	{
		public CaseSensitiveAttribute()
		{
			IsEnabled = true;
		}

		public bool IsEnabled { get; set; }
	}
}